import pyxel

from constants import BASE_Y

class Map:
    def __init__(self):
        self.far_cloud = [(-10, 55), (40, 45), (90, 40)]
        self.near_cloud = [(10, 15), (70, 25), (120, 5)]
        self.offset = 0

    def update_offset(self, speed):
        self.offset = self.offset + speed

    def draw(self):
        # draw sky
        pyxel.blt(0, 64, 0, 0, 88, 160, 32)

        # draw mountain
        pyxel.blt(0, 64, 0, 0, 64, 160, 24, 12)

        # draw forest
        offset = self.offset % 160
        for i in range(2):
            pyxel.blt(i * 160 - offset, 80, 0, 0, 48, 160, 16, 12)

        # draw clouds
        offset = (self.offset // 16) % 160
        for i in range(2):
            for x, y in self.far_cloud:
                pyxel.blt(x + i * 160 - offset, y, 0, 64, 32, 32, 8, 12)

        offset = (self.offset // 8) % 160
        for i in range(2):
            for x, y in self.near_cloud:
                pyxel.blt(x + i * 160 - offset, y, 0, 0, 32, 56, 8, 12)

        # draw ground
        offset = self.offset % 160
        for i in range(14):
            pyxel.blt(i * 32 - offset, BASE_Y + 16, 0, 0, 16, 32, 8, 12)

        pyxel.rect(0, 104, 160, 16, 0)
