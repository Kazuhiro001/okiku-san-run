from enum import Enum
from random import randint, random

import pyxel

from command import *
from constants import BASE_Y, FPS
from item import *
from map import Map
from player import Player


def is_collision(a, b):
    d = (a.size + b.size) / 2
    return abs(a.x - b.x) < d and abs(a.y - b.y) < d


class State(Enum):
    Playing = 0
    GameOver = 1


class ItemGenerator:
    def __init__(self):
        self.speed_rate = 0.1
        self.magnification_rate = 0.5
        self.score_rate = 1 - self.speed_rate - self.magnification_rate

    def init(self):
        items = []
        for (x, y) in [(i * 60, randint(0, BASE_Y)) for i in range(4)]:
            r = random()
            if r < self.speed_rate:
                items.append(SpeedItem(x, y))
            elif r < self.magnification_rate + self.speed_rate:
                items.append(MagnificationItem(x, y))
            else:
                items.append(ScoreItem(x, y))
        for (x, y) in [(i * 60, randint(0, BASE_Y)) for i in range(4)]:
            items.append(DamageItem(x + (random() * 10), y))
        return items

    def generate(self, player):
        r = random()
        if r < self.speed_rate and not player.isstar():
            return SpeedItem(randint(160, 200), randint(0, BASE_Y))
        elif r < self.magnification_rate + self.speed_rate:
            return MagnificationItem(randint(160, 200), randint(0, BASE_Y))
        else:
            return ScoreItem(randint(160, 200), randint(0, BASE_Y))


class Model:
    def __init__(self):
        self.state = State.Playing
        self.game_map = Map()
        self.player = Player()
        self.timer = 60
        self.score = 0
        self.magnification = 1
        self.display_score = 0
        self.score_pos = 40
        self.item_gen = ItemGenerator()
        self.items = self.item_gen.init()
        self.got_items = []
        self.cutin = None
        self.particles = []

    def update(self):
        next_actions = []
        self.player.update()
        self.update_items()
        self.update_particles()

        got_items = [
            i for i in self.items if is_collision(i, self.player)
            and not (isinstance(i, DamageItem) and self.player.isinvincible())
        ]
        for i in got_items:
            self.items.remove(i)
        if 0 < len(got_items):
            next_actions.append(PlaySoundCmd(4))
            self.player.action_count += 1
        for i in got_items:
            next_actions.extend(i.update_model(self))

        self.update_score()
        self.game_map.update_offset(self.player.speed())
        if pyxel.frame_count % FPS is 0:
            self.timer -= 1

        if self.cutin is not None:
            self.cutin.update()
            if self.cutin.time < 0:
                self.cutin = None

        return next_actions

    def update_items(self):
        for item in self.items:
            self._update_item_pos(item)

        self.items = [i for i in self.items if 0 <= i.x]

        if len([i for i in self.items if not isinstance(i, DamageItem)]) < 4:
            item = self.item_gen.generate(self.player)
            self.items.append(item)

        if len([i for i in self.items if isinstance(i, DamageItem)]) < 4:
            self.items.append(
                DamageItem(randint(160, 200), randint(0, BASE_Y)))

    def _update_item_pos(self, item):
        pos = np.array([float(item.x), float(item.y)])
        pos += np.array([-self.player.speed(), 0])
        if self.player.isstar() and not isinstance(item, DamageItem):
            player_v = np.array([float(self.player.x), float(self.player.y)])
            pos += (self.player.speed() + 1) * (
                player_v - pos) / np.linalg.norm(player_v - pos)
        item.x = pos[0]
        item.y = pos[1]

    def update_particles(self):
        for p in self.particles:
            p.update()

        self.particles = [p for p in self.particles if not p.isend()]

    def update_score(self):
        if self.display_score < self.score:
            self.display_score += 1


class App:
    def __init__(self):
        pyxel.init(160, 120, caption="Okiku san Run", fps=FPS)

        self.model = Model()
        self.actions = []

        pyxel.load("./my_resource.pyxres")

        pyxel.playm(1, loop=True)

        pyxel.run(self.update, self.draw)

    def update(self):
        if pyxel.btnp(pyxel.KEY_R):
            self.model = Model()
            return

        if self.model.state is State.GameOver:
            return

        if pyxel.btnp(pyxel.KEY_Z):
            self.actions.append(JumpCmd())

        if pyxel.btnp(pyxel.KEY_X):
            self.actions.append(DashCmd())

        next_actions = []
        for cmd in self.actions:
            next_actions.extend(cmd.execute(self.model))

        self.actions.clear()

        self.actions.extend(next_actions)
        self.actions.extend(self.model.update())

        snd_cmds = [c for c in self.actions if isinstance(c, PlaySoundCmd)]
        for c in snd_cmds:
            c.execute(self.model)
            self.actions.remove(c)

        if self._is_gameover(self.model):
            self.model.state = State.GameOver
            self.actions.clear()

    def _is_gameover(self, model):
        return model.timer < 0 or model.player.hp <= 0

    def draw(self):
        if self.model.state is State.Playing:
            self._draw_play_game()
        else:
            pyxel.cls(12)
            self.model.game_map.draw()
            s = "YOUR SCORE: {}".format(self.model.score)
            pyxel.text(51, 55, s, 1)
            pyxel.text(50, 55, s, 7)

            restart_text = "PRESS R TO RESTART"
            pyxel.text(45, 70, restart_text, 0)

    def _draw_play_game(self):
        pyxel.cls(12)

        self.model.game_map.draw()
        self.model.player.draw()
        for item in self.model.items:
            item.draw()
        for p in self.model.particles:
            p.draw()

        for i in range(3):
            if i < self.model.player.speed_lv - 1:
                pyxel.blt(5 + (i * 14), 104, 1, 32, 0, 16, 16, 11)
            else:
                pyxel.blt(5 + (i * 14), 104, 1, 48, 0, 16, 16, 11)

        s = "SCORE {:>4}".format(self.model.display_score)
        pyxel.text(5, 4, s, 1)
        pyxel.text(4, 4, s, 7)

        mag = "x {:>5}".format(self.model.magnification)
        pyxel.text(125, 110, mag, 10)

        pyxel.text(150, 4, "{}".format(self.model.timer), 7)

        if self.model.cutin is not None:
            self.model.cutin.draw()


App()
