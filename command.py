import pyxel
import numpy as np
from random import random, randint

from constants import FPS


class DashParticle:
    def __init__(self, pos, vel):
        self.pos = pos
        self.velocity = vel
        self.period = randint(FPS / 2, FPS)
        self.size = randint(0, 1)

    def update(self):
        self.period -= 1
        self.pos += self.velocity

    def draw(self):
        pyxel.circ(self.pos[0], self.pos[1], self.size, 7)

    def isend(self):
        return self.period < 0


class JumpCmd:
    def execute(self, model):
        pos = np.array([float(model.player.x), float(model.player.y) + 14])
        if model.player.jump():
            model.particles.extend(self._create_particles(pos))
            return [PlaySoundCmd(10)]
        else:
            return []

    def _create_particles(self, pos):
        r = []
        for x in range(-1, 2):
            for y in range(-1, 2):
                r.append(
                    DashParticle(pos + np.array([x, y]),
                                 np.array([-random(), 0])))
        return r


class DashCmd:
    def execute(self, model):
        pos = np.array([float(model.player.x), float(model.player.y) + 14])
        if model.player.dash():
            vel = np.array([-random() * model.player.vx, 0]) / 3
            model.particles.extend(self._create_particles(pos, vel))
        return [PlaySoundCmd(10)]

    def _create_particles(self, pos, vel):
        r = []
        for x in range(-1, 4):
            for y in range(-1, 2):
                r.append(DashParticle(pos + np.array([x, y]), vel))
        return r


class SoundManager:
    def __init__(self):
        self.prev_frame = 0

    def play(self, snd):
        if snd is 9:
            pyxel.play(3, snd)
            self.prev_frame = pyxel.frame_count
        elif FPS < pyxel.frame_count - self.prev_frame:
            pyxel.play(3, snd)


sound_manager = SoundManager()


class PlaySoundCmd:
    def __init__(self, snd):
        self.snd = snd

    def execute(self, model):
        sound_manager.play(self.snd)
        return []
