お菊さんが走るゲーム
==========================

## 実行方法

[python](https://www.python.org/)がインストールされている環境で
以下のコマンドで実行されます。

```
pip install pipenv
pipenv install
pipenv run start
```

## 操作方法

キーボード操作のみです。

- `Z キー`
    - ジャンプ(2 段ジャンプ可能)
- `X キー`
    - ダッシュする
