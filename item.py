from random import randint, random

import numpy as np
import pyxel

from command import *
from effect import Cutin


class Item:
    def __init__(self, x, y, size):
        self.x = x
        self.y = y
        self.size = size

    def update_model(self, model):
        pass

    def draw(self):
        pyxel.blt(self.x, self.y, 1, 0, 0, 16, 16, 11)


class SpeedItem(Item):
    def __init__(self, x, y):
        super().__init__(x, y, 12)
        self.up_speed = 1

    def update_model(self, model):
        actions = []
        if model.player.ismaxspeed():
            return actions
        model.player.speed_lv += 1
        if model.player.ismaxspeed():
            model.player.star_dash()
            model.cutin = Cutin()
            actions.append(PlaySoundCmd(9))
        return actions

    def draw(self):
        if self.up_speed is 1:
            pyxel.blt(self.x, self.y, 1, 32, 0, 16, 16, 11)
        else:
            pyxel.blt(self.x, self.y, 1, 16, 0, 16, 16, 11)


class MagnificationItem(Item):
    def __init__(self, x, y):
        super().__init__(x, y, 12)
        self.kind = randint(0, 3)

    def update_model(self, model):
        if self.kind is 0:
            model.magnification *= 2
        else:
            model.magnification += 1
        return []

    def draw(self):
        if self.kind is 0:
            pyxel.blt(self.x, self.y, 1, 0, 16, 16, 16, 11)
        else:
            pyxel.blt(self.x, self.y, 1, 32, 16, 16, 16, 11)



class Particle:
    def __init__(self, size, pos, target_pos):
        self.pos = np.array([float(pos[0]), float(pos[1])])
        self.target_pos = target_pos
        self.velocity = np.array([random() * 2 - 1, random() * 2 - 1]) * 3
        self.size = size
        self.period = float(FPS)

    def update(self):
        if self.isend():
            return
        diff = self.target_pos - self.pos
        acc = (diff - self.velocity * self.period) * 2 / (
            self.period * self.period)
        self.period -= 1
        self.velocity += acc
        self.pos += self.velocity

    def draw(self):
        pyxel.circ(self.pos[0], self.pos[1], self.size, 9)

    def isend(self):
        return self.period <= 0


class ScoreItem(Item):
    def __init__(self, x, y):
        super().__init__(x, y, 12)

    def update_model(self, model):
        gain_score = model.magnification * 1
        model.score += gain_score
        pos = np.array([self.x, self.y])
        target_pos = np.array([model.score_pos, 6])
        model.particles.extend(
            self._create_particles(gain_score, pos, target_pos))
        return []

    def _create_particles(self, score, pos, target_pos):
        if score < 50:
            max_size = 1
            amount = 5
        elif score < 1000:
            max_size = 2
            amount = 7
        else:
            max_size = 3
            amount = 10

        return [
            Particle(randint(0, max_size), pos, target_pos)
            for i in range(amount)
        ]

    def draw(self):
        pyxel.blt(self.x, self.y, 1, 0, 32, 16, 16, 11)


class DamageItem(Item):
    def __init__(self, x, y):
        super().__init__(x, y, 8)

    def update_model(self, model):
        model.player.hp -= 1
        return [PlaySoundCmd(5)]

    def draw(self):
        frame = (pyxel.frame_count // 3) % 4
        pyxel.blt(self.x, self.y, 1, frame * 16, 48, 16, 16, 11)
