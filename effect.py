from random import random

import pyxel

from constants import FPS


class Cutin:
    TIME_IN = 0.2
    TIME_OUT = 0.2
    TIME_TOTAL = 1

    def __init__(self):
        self.time = Cutin.TIME_TOTAL * FPS

    def update(self):
        self.time -= 1

    def draw(self):
        elapsedTime = Cutin.TIME_TOTAL * FPS - self.time
        if elapsedTime < Cutin.TIME_IN * FPS:
            pyxel.blt(0, random() * 5, 2, 0, 0, 160, 120, 11)
            offset = (60 / (Cutin.TIME_IN * FPS)) * (
                Cutin.TIME_IN * FPS - elapsedTime)
            pyxel.rect(0, 0, 160, offset, 7)
            pyxel.rect(0, 120 - offset, 160, offset, 7)
        elif self.time < 0.2 * FPS:
            offset = (60 / (Cutin.TIME_OUT * FPS)) * self.time
            pyxel.clip(0, 60 - offset, 160, offset * 2)
            pyxel.blt(0, 0, 2, 0, 0, 160, 120, 11)
            pyxel.clip()
        else:
            pyxel.blt(0, 0, 2, 0, 0, 160, 120, 11)
