import pyxel

from constants import BASE_Y, FPS


class Player:
    BASE_X = 16
    MAX_SPEED = 4

    def __init__(self):
        self.x = Player.BASE_X
        self.y = BASE_Y
        self.size = 12
        self.vx = 0
        self.vy = 0
        self.friction = 1
        self.gravity = 1
        self.action_count = 2
        self.speed_lv = 1
        self.hp = 1
        self.star_dash_period = 0

    def update(self):
        if self.y < BASE_Y:
            if not (0 < self.vy
                    and 0 < self.vx) and not 0 < self.star_dash_period:
                self.vy += self.gravity
                self.y += self.vy

        if BASE_Y <= self.y:
            self.y = BASE_Y
            self.vy = 0
            self.action_count = 2

        if -1 < self.vx:
            self.vx -= self.friction

        if Player.BASE_X < self.x:
            self.x += self.vx

        if self.x <= Player.BASE_X:
            self.x = Player.BASE_X
            self.vx = 0

        if 0 < self.star_dash_period:
            self.star_dash_period -= 1
        elif Player.MAX_SPEED <= self.speed_lv:
            self.speed_lv = 1

    def draw(self):
        if self.isstar():
            frame = (pyxel.frame_count // 3) % 2
            pyxel.blt(self.x, self.y, 0, 96 + frame * 16, 0, 16, 16, 11)
        else:
            frame = (pyxel.frame_count // 3) % 6
            if self.action_count == 0:
                pyxel.pal(0, 13)
                pyxel.blt(self.x, self.y, 0, frame * 16, 0, 16, 16, 11)
                pyxel.pal()
            else:
                pyxel.blt(self.x, self.y, 0, frame * 16, 0, 16, 16, 11)

    def jump(self):
        if 0 < self.action_count:
            self.vy = -9
            self.y += self.vy
            self.action_count -= 1
            return True
        return False

    def dash(self):
        if 0 < self.action_count:
            self.vx = 9
            self.x += self.vx
            self.action_count -= 1
            return True
        return False

    def star_dash(self):
        self.star_dash_period = 3 * FPS
        self.vx = 9

    def speed(self):
        if self.vx < 0:
            return self.speed_lv - self.vx
        else:
            return self.speed_lv

    def isinvincible(self):
        return self.isstar() or 0 < self.vx

    def isstar(self):
        return 0 < self.star_dash_period

    def ismaxspeed(self):
        return self.speed_lv is Player.MAX_SPEED
